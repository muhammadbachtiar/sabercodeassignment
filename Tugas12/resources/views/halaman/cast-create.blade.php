@extends('layout.master')
@section('judul')
Tambah Cast
@endsection
@section('content')
<h1>Tambah Cast Baru</h1>
    <form action="/cast" method="post">
        @csrf
        <div class="mb-3">
            <label for="nama" class="form-label">Nama</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" placeholder="Nama...">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="mb-3">
            <label for="umur" class="form-label">Umur</label>
            <input type="number" min="1" max="200" class="form-control @error('umur') is-invalid @enderror" id="umur" name="umur" placeholder="Umur...">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="mb-3">
            <label for="bio" class="form-label">Bio</label>
            <textarea class="form-control @error('bio') is-invalid  @enderror" id="bio" name="bio" rows="3"  cols="25"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection