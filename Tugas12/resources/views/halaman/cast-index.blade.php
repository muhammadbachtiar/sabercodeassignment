@extends('layout.master')
@section('judul')
Data Cast
@endsection
@push('scripts')
    <script src="{{asset('/templateAdmin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('/templateAdmin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<a href="/cast/create" class="btn btn-primary my-3">Tambah Cast</a>
<table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Aksi</th>
    </tr>
    </thead>
    <tbody>
    @if(count($cast) > 0)
    @foreach ($cast as $key => $item)
    <tr>
        <td>{{$key + 1}}</td>
        <td>{{$item->nama}}</td>
        <td>{{$item->umur}}</td>
        <td>
        <form action="/cast/{{$item->id}}" method="post">
            @csrf
            @method('delete')
            <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
        </form>
        </td>
    </tr>
    @endforeach
    @else
        <tr>
            <td colspan="4">Tidak Ada Data</td>
        </tr>
    @endif
    </tbody>
    <tfoot>
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Aksi</th>
    </tr>
    </tfoot>
  </table>
@endsection