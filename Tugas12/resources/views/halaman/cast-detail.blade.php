@extends('layout.master')
@section('judul')
Detail Cast
@endsection
@section('content')
    <h2 class="text-primary">{{$cast->nama}}</h2>
    <h3>Umur</h3>
    <p>{{$cast->umur}} Tahun</P>
    <h3>Bio</h3>
    <p>{{$cast->bio}}</P>
@endsection