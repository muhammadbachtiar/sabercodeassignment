@extends('layout.master')
@section('judul')
Register Account
@endsection
@section('content')
<h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/welcome" method="post" >
        @csrf
        <label>First Name :</label><br><br>
        <input type="text" name="firstName" required><br><br>
        <label>Last Name :</label><br><br>
        <input type="text" name="lastName"><br><br>
        <label>Gender :</label><br><br>
        <input type="radio" name="gender" value="Male" checked>Male<br>
        <input type="radio" name="gender" value="Female">Female<br>
        <input type="radio" name="gender" value="Other">Other<br><br>
        <label>Nationality :</label><br><br>
        <select name="nationality" id="nationality" required>
            <option value="indonesia" value="Indonesia">Indonesia</option>
            <option value="usa" value="United States">United States</option>
            <option value="uk" value="United Kingdom">United Kingdom</option>
            <option value="australia" value="Australia">Australia</option>
        </select> <br><br>
        <label>Language Spoken :</label><br><br>
        <input type="checkbox" name="language[]" value="Bahasa Indonesia" checked>Bahasa Indonesia<br>
        <input type="checkbox" name="language[]" value="English">English<br>
        <input type="checkbox" name="language[]" value="Other" >Other<br><br>
        <label>Bio:</label><br><br>
        <textarea  name="bio" rows="4" cols="25"></textarea><br><br>

        <input type="submit" value="Regist">
    </form>
@endsection