@extends('layout.master')
@section('judul')
Welcome
@endsection
@section('content')
<h1>SELAMAT DATANG! {{ strtoupper($firstName . ' ' . $lastName) }}</h1>
<P>Profil Anda :</p>
    <table>
        <tr>
            <td>Jenis Kelamin </td>
            <td>: {{$gender}}</td>
        </tr>
        <tr>
            <td>Kebangsaan</td>
            <td>: {{$nationality}}</td>
        </tr>
        <tr>
            <td>Bahasa </td>
            <td>
                <ul>
                    @for ($i = 0; $i < count($language); $i++)
                        <li>{{ $language[$i] }}</li>
                    @endfor
                </ul>
            </td>
        </tr>
        <tr>
            <td>Bio</td>
            <td>: {{$bio}}</td>
        </tr>
    </table>
<h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
@endsection