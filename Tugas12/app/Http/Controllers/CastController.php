<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
        return view('halaman.cast-index', ['cast' => $cast]);
    }

    public function create(){
        return view('halaman.cast-create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        DB::table('cast')->insert([
            'nama' =>$request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);

        return redirect('/cast');
    }

    public function show($cast_id){
        $cast = DB::table('cast')->find($cast_id);
        return view('halaman.cast-detail', ['cast' => $cast]);
    }

    public function edit($cast_id){
        $cast = DB::table('cast')->find($cast_id);
        return view('halaman.cast-edit', ['cast' => $cast]);
    }

    public function update($cast_id, Request $request){
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        DB::table('cast')
              ->where('id', $cast_id)
              ->update([
                    'nama' => $request->input('nama'),
                    'umur' => $request->input('umur'),
                    'bio' => $request->input('bio')
                ]);

        return redirect('/cast');
    }

    public function destroy($cast_id){
        DB::table('cast')->where('id', '=', $cast_id)->delete();
        return redirect('/cast');
    }
}
