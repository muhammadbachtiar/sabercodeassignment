<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function toRegister(){


        return view('halaman.register');
    }

    public function completeRegister(Request $request){
        $firstName = $request["firstName"];
        $lastName = $request["lastName"];
        $gender = $request["gender"];
        $nationality = $request["nationality"];
        $language = $request["language"];
        $bio = $request["bio"];

        return view('halaman.welcome',['firstName' => $firstName, 'lastName' => $lastName, 'gender' => $gender, 'nationality' => $nationality, 'language' => $language,'bio' => $bio]);
    }

    public function toTabel(){


        return view('halaman.table');
    }

    public function toDataTabel(){


        return view('halaman.data-table');
    }
}
