<?php
require_once 'animal.php';
require_once 'Ape.php';
require_once 'Frog.php';

$sheep = new Animal("shaun");

echo "Name :" . $sheep->get_name() ."<br>"; // "shaun"
echo "Legs :" . $sheep->get_legs() ."<br>"; // 4
echo "Cold Blooded :" . $sheep->get_cold_blooded() ."<br>"; // "no"

echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name :" . $sungokong->get_name() ."<br>"; 
echo "Legs :" . $sungokong->get_legs() ."<br>"; 
echo "Cold Blooded :" . $sungokong->get_cold_blooded() ."<br>"; 
echo "Yell :" . $sungokong->yell() ."<br>"; // "Auooo"

echo "<br>";

$kodok = new Frog("buduk");
echo "Name :" . $kodok->get_name() ."<br>";
echo "Legs :" . $kodok->get_legs() ."<br>"; 
echo "Cold Blooded :" . $kodok->get_cold_blooded() ."<br>";
echo "Jump :" .$kodok->jump() ."<br>"; // "hop hop"
?>